import { IResponse } from 'helpers/axiosClient';
import useRequest from 'hooks/base/axios/useRequest';
import { useState } from 'react';

export type Address = {
  id: number;
  name: string;
  type: 'province';
  parentId: null | number;
  isFromBooking: boolean;
};

type ResAddress = IResponse & {
  data?: Address[];
};

interface Err extends Error {}

function useListAddress() {
  const request = useRequest();
  const [params, setParams] = useState({ limit: 10, page: 1, filterType: 'province' });
  const [{ response, loading, error }] = request<ResAddress, Err>({
    url: 'common/address',
    params: params
  });

  return {
    params,
    setParams,
    response,
    loading,
    error
  };
}

export default useListAddress;
