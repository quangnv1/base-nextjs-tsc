import { makeUseAxios } from 'axios-hooks';
import { instance } from 'helpers/axiosClient';

const useRequest = () => {
  return makeUseAxios({
    axios: instance,
    cache: false
  });
};

export default useRequest;
