import { signIn } from 'next-auth/client';
import { useState } from 'react';

export type DataLogin = {
  username: string;
  password: string;
};

function useLogin() {
  const [logging, setLogging] = useState(false);
  const login = async (data: DataLogin, type?: string) => {
    setLogging(true);
    signIn(type, { ...data, redirect: false });
    setLogging(false);
  };

  return {
    logging,
    login
  };
}

export default useLogin;
