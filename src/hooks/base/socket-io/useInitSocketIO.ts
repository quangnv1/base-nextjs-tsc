import { useEffect, useRef } from 'react';
import SocketIOClient from 'socket.io-client';
import { useSession } from 'next-auth/client';

export const socketIOClient = (accessToken?: string) =>
  SocketIOClient(process.env.NEXT_PUBLIC_SERVER_SOCKET_IO || 'ws://localhost:4000', {
    transports: ['polling'],
    query: { accessToken }
  });

function useInitSocketIO() {
  const [session] = useSession();
  const ioClient = useRef<SocketIOClient.Socket>();
  useEffect(() => {
    ioClient.current && ioClient.current.disconnect();
    ioClient.current = socketIOClient(session?.user?.accessToken);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ioClient.current.on('message', (data: any) => {
      console.log(data);
    });
    return () => {
      ioClient.current && ioClient.current.disconnect();
    };
  }, [session]);
}

export default useInitSocketIO;
