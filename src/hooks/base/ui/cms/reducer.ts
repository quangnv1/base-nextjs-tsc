import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localStorageHelper, { KeyStorage } from 'helpers/localStorage';
import _ from 'lodash';
export type UiCmsState = {
  header?: {
    margin: string;
    height?: string;
    show?: boolean;
  };
  sider?: {
    show?: boolean;
    width?: string;
    collapsedWidth?: string;
  };
};
const initialState: UiCmsState = _.merge(
  {
    header: {
      height: '4em',
      margin: '.5em',
      show: true
    },
    sider: {
      show: true,
      width: '250px',
      collapsedWidth: '50px'
    }
  },
  localStorageHelper.getObject(KeyStorage.UI_CMS, null) || {}
);

const uiCms = createSlice({
  name: 'uiCms',
  initialState: initialState,
  reducers: {
    changeUiCms: (state: UiCmsState, action: PayloadAction<UiCmsState>) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = uiCms;
export const { changeUiCms } = actions;
export default reducer;
