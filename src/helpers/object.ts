type ObjectSortByKey = {
  [key: string]: string | number | boolean | undefined | ObjectSortByKey | ObjectSortByKey[];
};
export const sortObjectByKey = (
  object: ObjectSortByKey,
  type: 'asc' | 'desc' = 'asc'
): ObjectSortByKey => {
  const newObject: ObjectSortByKey = {};
  const keys = Object.keys(object).sort();
  if (type === 'desc') {
    keys.reverse();
  }
  keys.forEach((key: string) => (newObject[key] = object[key]));
  return newObject;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function trimAll(data: any): any {
  if ([null, undefined, true, false].indexOf(data) >= 0) {
    return data;
  }
  if (typeof data === 'string') {
    return data.trim();
  }
  if (Array.isArray(data)) {
    return data.map((x) => trimAll(x));
  }
  if (typeof data === 'object') {
    const newObject = { ...data };
    Object.keys(newObject).map((k) => {
      console.log(k, typeof newObject[k]);
      newObject[k] = trimAll(newObject[k]);
    });
    return newObject;
  }
  return data;
}
