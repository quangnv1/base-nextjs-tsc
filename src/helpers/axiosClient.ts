/* eslint-disable @typescript-eslint/no-explicit-any */
import { message } from 'antd';
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import statusCode from 'http-status-codes';
import queryString from 'query-string';
import { getSession } from 'next-auth/client';

export type Data = {
  [key: string]: Data | number | string | undefined | null;
};
export type IRequest = {
  url: string;
  params: Data;
  data: Data;
};
export type IResponse = {
  statusCode: number;
  message: string;
  data?: Data;
};

export const getHeader = (customHeaders: any = {}): any => {
  const session = getSession();
  if (session) {
    const token = session.user?.accessToken;
    if (token) {
      return {
        ...customHeaders,
        Authorization: `Bearer ${token}`
      };
    }
  }
  return { ...customHeaders };
};

export const instance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  timeout: 10000,
  paramsSerializer: (params) => queryString.stringify(params)
});

instance.defaults.headers = getHeader();

const requestHandler = (request: AxiosRequestConfig) => {
  console.log(`>>> Request API: ${request.url}`);
  console.log(`  + Params:     `, request.params);
  console.log(`  + Data:       `, request.data);
  return request;
};

const successHandler = (response: AxiosResponse) => {
  console.log(`<<< Response API: ${response.config.url}`, response.data);
  return response.data;
};

const errorHandler = async (error: AxiosError) => {
  message.error(error.message);
  return Promise.reject(error);
};

instance.interceptors.request.use((request: AxiosRequestConfig) => {
  return requestHandler(request);
});

instance.interceptors.response.use(
  (response: AxiosResponse) => {
    return successHandler(response);
  },
  (error: AxiosError) => {
    return errorHandler(error);
  }
);

const axiosClient = {
  get: async <ReqType, ResType extends IResponse>(
    url: string,
    params?: ReqType,
    customHeaders: any = {}
  ): Promise<ResType> => {
    const headers = getHeader(customHeaders);
    return instance.get(url, { params, headers });
  },
  post: async <ReqType, ResType extends IResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
  ): Promise<ResType> => {
    const headers = getHeader(customHeaders);
    return instance.post(url, data, { headers });
  },
  put: async <ReqType, ResType extends IResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
  ): Promise<ResType> => {
    const headers = getHeader(customHeaders);
    return instance.put(url, data, { headers });
  },
  delete: async <ReqType, ResType extends IResponse>(
    url: string,
    params?: ReqType,
    customHeaders: any = {}
  ): Promise<ResType> => {
    const headers = getHeader(customHeaders);
    return instance.delete(url, { params, headers });
  },
  statusCode: statusCode
};

export default axiosClient;
