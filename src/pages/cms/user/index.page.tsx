import dynamic from 'next/dynamic';
import IDatePicker from 'pages/components/base/IDatePicker';
import { withSSAuth } from 'pages/components/hocs/withSSAuth';
import { withSSTranslations } from 'pages/components/hocs/withSSTranslations';
import MasterLayout from '../layouts/MasterLayout';

const IBraftEditor = dynamic(() => import('pages/components/base/IBraftEditor'), { ssr: false });
function IndexUserPage() {
  return (
    <div>
      <IDatePicker />
      <IBraftEditor />
    </div>
  );
}

IndexUserPage.Layout = MasterLayout;

export const getServerSideProps = withSSAuth(withSSTranslations());

export default IndexUserPage;
