import { Button, Select, Space } from 'antd';
import useListAddress from 'hooks/address/useListAddress';
import ChangeLocale from 'pages/components/base/ChangeLocale';
import { useEffect } from 'react';
import MasterLayout from './layouts/MasterLayout';
import { withSSTranslations } from 'pages/components/hocs/withSSTranslations';

function IndexPage() {
  const { params, setParams, response, loading, error } = useListAddress();

  useEffect(() => {
    console.log('loading', loading);
  }, [loading]);

  useEffect(() => {
    console.log('response', response);
  }, [response]);

  useEffect(() => {
    console.log('response', error);
  }, [error]);

  return (
    <Space>
      <ChangeLocale />
      <Select>
        <Select.Option value="0">Hello</Select.Option>
        <Select.Option value="1">3</Select.Option>
      </Select>
      <Button
        onClick={() =>
          params?.page > 1 && setParams({ ...params, page: Number(params?.page || 0) - 1 })
        }>
        PrePage
      </Button>
      <Button
        onClick={() => {
          setParams({ ...params, page: Number(params?.page || 0) + 1 });
        }}>
        NextPage
      </Button>
    </Space>
  );
}

IndexPage.Layout = MasterLayout;

export const getStaticProps = withSSTranslations();

export default IndexPage;
