import { Layout } from 'antd';
import useInitPage from 'hooks/base/page/useInitPage';
import usePage from 'hooks/base/page/usePage';
import useInitUiCms from 'hooks/base/ui/cms/useInitUiCms';
import dynamic from 'next/dynamic';
import Loading from 'pages/components/base/Loading';
import NotFound from 'pages/components/base/NotFound';
import { RootState } from 'pages/store';
import { useSelector } from 'react-redux';
import ContentLayout from './content/ContentLayout';
import HeaderLayout from './header/HeaderLayout';

const SiderLayout = dynamic(() => import('./sider/SiderLayout'), { ssr: false });

type Props = {
  children?: JSX.Element | React.ReactNode;
};

function MasterLayout(props: Props) {
  const { page } = usePage();
  useInitPage();
  useInitUiCms();
  const nprogress = useSelector((state: RootState) => state.nprogress);
  const { children } = props;

  return (
    <Layout style={{ height: '100vh' }}>
      <SiderLayout />
      <Layout style={{ height: '100vh' }}>
        <HeaderLayout />
        <Loading loading={nprogress.status === 'start'} size="3em" />
        <ContentLayout hidden={nprogress.status === 'start'}>
          {page.isNotFound ? <NotFound /> : children}
        </ContentLayout>
      </Layout>
    </Layout>
  );
}

export default MasterLayout;
