import { Button, Card, Checkbox, Form, Input } from 'antd';
import useLogin, { DataLogin } from 'hooks/base/auth/useLogin';
import { getSession, useSession } from 'next-auth/client';
import { useRouter } from 'next/router';
import ElementCenter from 'pages/components/base/ElementCenter';
import { withSSTranslations } from 'pages/components/hocs/withSSTranslations';
import { RootState } from 'pages/store';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 }
};

const LoginPage = ({ query }) => {
  const { t } = useTranslation();
  const router = useRouter();
  const [session, sessionLoading] = useSession();
  const { logging, login } = useLogin();
  const { loading } = useSelector((state: RootState) => state.nprogress);

  useEffect(() => {
    if (!sessionLoading) {
      if (session) {
        router.replace(query?.redirect || '/');
      }
    }
  }, [session, sessionLoading]);

  const onFinish = (values: DataLogin) => {
    login(
      {
        username: values.username,
        password: values.password
      },
      'credentials'
    );
  };

  return (
    <ElementCenter style={{ height: '100vh' }}>
      <Card headStyle={{ textAlign: 'center' }} title={t('login')}>
        <Form {...layout} name="basic" initialValues={{ remember: true }} onFinish={onFinish}>
          <Form.Item label={t('form:labels.username')} name="username" rules={[{ required: true }]}>
            <Input />
          </Form.Item>

          <Form.Item label={t('form:labels.password')} name="password" rules={[{ required: true }]}>
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout} name="remember" valuePropName="checked">
            <Checkbox>{t('form:labels.remember_me')}</Checkbox>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button loading={logging || loading} type="primary" htmlType="submit">
              {t('form:labels.login')}
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </ElementCenter>
  );
};

export const getServerSideProps = withSSTranslations(['form'], (ctx) => {
  return new Promise((resolve) => {
    getSession(ctx).then((session) => {
      if (session) {
        resolve({
          redirect: {
            destination: ctx.query?.redirect.toString() || '/',
            permanent: false
          }
        });
      }
      resolve({
        props: {
          query: ctx.query
        }
      });
    });
  });
});

export default LoginPage;
