import { useSession } from 'next-auth/client';
import { useRouter } from 'next/router';
import React from 'react';

interface Config {
  permissions: Array<string | number>;
  redirect?: string;
}

// eslint-disable-next-line react/display-name
const withPermission = (WrappedComponent) => (config: Config) => () => {
  const router = useRouter();
  const { session } = useSession();
  if (session?.user?.accessToken) {
    return <WrappedComponent />;
  }
  if (config?.redirect) {
    try {
      router.push(config.redirect);
      // eslint-disable-next-line no-empty
    } catch (error) {}
  }
  return <></>;
};

export default withPermission;
