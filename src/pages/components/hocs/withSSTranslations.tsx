import { merge } from 'lodash';
import { GetServerSideProps } from 'next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

const defaultNamespaces = ['translations', 'messages', 'form', 'theme'];

export function withSSTranslations(namespaces: string[] = [], cb?: GetServerSideProps) {
  return async (context) => {
    return merge(
      {
        props: {
          ...(await serverSideTranslations(context.locale, [...defaultNamespaces, ...namespaces]))
        }
      },
      cb ? cb(context) : { props: {} }
    );
  };
}
