import { GetServerSideProps } from 'next';
import { stringify } from 'query-string';
import { getSession } from 'next-auth/client';

export function withSSAuth(cb?: GetServerSideProps) {
  return async (context) => {
    const session = await getSession(context);
    if (!session) {
      return {
        redirect: {
          destination: `/auth/login?${stringify({ redirect: context.resolvedUrl })}`,
          permanent: false
        }
      };
    }
    return cb ? cb(context) : { props: {} };
  };
}
