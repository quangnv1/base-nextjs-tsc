import { Autocomplete, GoogleMap, LoadScriptNext, Marker } from '@react-google-maps/api';
import { Libraries } from '@react-google-maps/api/dist/utils/make-load-script-url';
import { Input } from 'antd';
import React, { useRef, useState } from 'react';

const containerStyle = {
  width: '100%',
  height: '400px'
};

const libraries: Libraries = ['places'];

function MyGoogleMap() {
  const [pick, setPick] = useState({ lat: 21.0031177, lng: 105.8201408 });

  const autocompleteRef = useRef<any>();

  const onLoad = (autocomplete) => {
    autocompleteRef.current = autocomplete;
  };

  const onPlaceChanged = () => {
    if (autocompleteRef.current) {
      const place = autocompleteRef.current?.getPlace();
      if (place.geometry?.location) {
        setPick({ lat: place.geometry.location.lat(), lng: place.geometry.location.lng() });
      }
    } else {
      console.log('Autocomplete is not loaded yet!');
    }
  };

  return (
    <LoadScriptNext
      libraries={libraries}
      googleMapsApiKey="AIzaSyAzT63S9denkIRaQ4-FrtzNAiB7haolovg">
      <GoogleMap
        mapContainerClassName="google-map"
        mapContainerStyle={containerStyle}
        options={{ disableDefaultUI: true }}
        center={pick}
        zoom={12}>
        <Autocomplete onLoad={onLoad} onPlaceChanged={onPlaceChanged} className="map-autocomplete">
          <Input type="text" placeholder="Input search" />
        </Autocomplete>
        <Marker
          animation={2}
          onDragEnd={(e) => console.log(e)}
          draggable={true}
          visible={true}
          position={pick}
        />
        <></>
      </GoogleMap>
    </LoadScriptNext>
  );
}

export default React.memo(MyGoogleMap);
