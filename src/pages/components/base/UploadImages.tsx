import { PlusOutlined } from '@ant-design/icons';
import { Modal, Upload } from 'antd';
import { UploadChangeParam, UploadFile } from 'antd/lib/upload/interface';
import axiosClient, { getHeader } from 'helpers/axiosClient';
import { uniqueId } from 'lodash';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

async function getBase64(file?: File | Blob) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file as Blob);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}
type StateUpload = {
  previewVisible: boolean;
  previewImage: string;
  previewTitle: string;
  fileList: UploadFile[];
};

type Props = {
  length?: number;
  onChangeFile?: (file: UploadFile) => void;
  initUrls?: string[];
  action?: string;
};

function UploadImages(props: Props) {
  const { length, onChangeFile, action } = props;
  const { t } = useTranslation();
  const [state, setState] = useState<StateUpload>({
    previewVisible: false,
    previewImage: '',
    previewTitle: '',
    fileList: []
  });

  useEffect(() => {
    const initFileList =
      props?.initUrls?.slice(0 - (length || 1))?.map((item) => {
        return {
          uid: uniqueId(),
          name: item,
          size: 1000,
          type: 'image/*',
          url: item
        };
      }) || [];
    setState({ ...state, fileList: [...initFileList] as UploadFile[] });
  }, [props.initUrls]);

  const handleCancel = () => setState({ ...state, previewVisible: false });

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handlePreview = async (file: UploadFile<any>) => {
    if (!file.url && !file.preview) {
      file.preview = (await getBase64(file.originFileObj)) as string;
    }
    setState({
      ...state,
      previewImage: String(file.url || file.preview),
      previewVisible: true,
      previewTitle: String(file.name || file?.url?.substring(file?.url.lastIndexOf('/') + 1))
    });
  };

  const handleChange = (info: UploadChangeParam) => {
    let fileList = [...info.fileList];
    fileList = fileList.slice(0 - (length || 1));
    fileList = fileList.map((file) => {
      if (file.response) {
        const res = file.response;
        if (res.statusCode === axiosClient.statusCode.OK) {
          file.url = file.response.data[0].url;
        }
      }
      return file;
    });
    setState({ ...state, fileList: fileList });
    onChangeFile && onChangeFile(info.file);
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>{t('upload')}</div>
    </div>
  );

  return (
    <>
      <Upload
        name="files"
        multiple={(length || 1) > 1 ? true : false}
        listType="picture-card"
        fileList={state.fileList}
        onPreview={handlePreview}
        onChange={handleChange}
        action={action || `${process.env.NEXT_PUBLIC_API_URL}/common/file/upload-temp`}
        headers={getHeader()}>
        {state.fileList.length >= (length || 1) ? null : uploadButton}
      </Upload>
      <Modal
        visible={state.previewVisible}
        title={state.previewTitle}
        footer={null}
        onCancel={handleCancel}>
        <img alt={state.previewTitle} style={{ width: '100%' }} src={state.previewImage} />
      </Modal>
    </>
  );
}

export default UploadImages;
