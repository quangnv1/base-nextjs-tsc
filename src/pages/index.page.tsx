import Container from 'pages/components/base/Container';
import { withSSTranslations } from 'pages/components/hocs/withSSTranslations';
import MasterLayout from 'pages/layouts/MasterLayout';
import GoogleMap from './components/base/GoogleMap';

function IndexPage() {
  return (
    <Container style={{ position: 'relative' }}>
      <>
        <GoogleMap />
      </>
    </Container>
  );
}

IndexPage.Layout = MasterLayout;

export const getStaticProps = withSSTranslations();

export default IndexPage;
