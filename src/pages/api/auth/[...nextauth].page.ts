import NextAuth from 'next-auth';
import Providers from 'next-auth/providers';

type AuthCredentials = { username: string; password: string };

export default (req, res) => {
  return NextAuth(req, res, {
    providers: [
      Providers.Credentials({
        id: 'credentials',
        name: 'credentials',
        credentials: {
          username: { label: 'Username', type: 'text' },
          password: { label: 'Password', type: 'password' }
        },
        async authorize(credentials: AuthCredentials) {
          const user = {
            id: 1,
            avatar: null,
            name: 'Thắng',
            email: 'mvt1904@gmail.com',
            accessToken: 'accessToken',
            refreshToken: 'refreshToken',
            ...credentials
          };
          if (user) {
            return user;
          } else {
            throw new Error('Login false');
          }
        }
      })
    ],
    session: {
      jwt: true
    },
    callbacks: {
      async session(session, user) {
        session.user = user;
        return session;
      },
      async jwt(token, user) {
        return { ...token, ...user };
      }
    }
  });
};
