import { DatePicker } from 'antd';
import dynamic from 'next/dynamic';
import Container from 'pages/components/base/Container';
import MasterLayout from 'pages/layouts/MasterLayout';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
import { withSSTranslations } from 'pages/components/hocs/withSSTranslations';

const ChangeLocale = dynamic(() => import('pages/components/base/ChangeLocale'), { ssr: false });

function IndexProductPage() {
  const { t } = useTranslation();
  return (
    <Container style={{ position: 'relative' }}>
      <MasterLayout>
        <Link href="/">{t('home')}</Link>
        <Link href="/admin">{t('admin')}</Link>
        <br />
        <DatePicker />
        <br />
        <ChangeLocale />
      </MasterLayout>
    </Container>
  );
}

export const getStaticProps = withSSTranslations();

export default IndexProductPage;
